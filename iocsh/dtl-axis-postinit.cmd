#==============================================================================
# 

#-d /**
#-d   \brief Configure Record fields for DTL motor axis.
#-d   \details Set enable condition for axis using SDIS and DISV record fields.
#-d   \author Maurizio Montis (INFN-LNL)
#-d   \file
#-d   \param P Section name, i.e. DTL-010
#-d   \param IDX Device position (accordig to ESS Naming Convetion indexing)
#-d   \param ENABLE_COND Enable condition value, i.e. 0
#-d   \note Example call:
#-d   \code
#-d    
#-d   \endcode
#-d */


afterInit(dbpf "$(P):EMR-SM-$(IDX):Axis.SDIS" "$(P):EMR-SM-$(IDX):EnbCond")
afterInit(dbpf "$(P):EMR-SM-$(IDX):Axis.DISV" "$(ENABLE_COND)")

